
# Neo4j-grapher


*Визуализация данных в виде 3D и 2D графов*

Иногда лучше один раз увидеть, чем сто раз услышать. Особенно, когда речь идет о таких разделах математики,  как теория графов. 


**Примеры**

* [Home](https://neo4j-grapher.now.sh/)
  
* [Javascript basic](https://neo4j-grapher.now.sh/?tabs=tabs/js-basic.json&runjavascript)

* [Javascript grid](https://neo4j-grapher.now.sh/?tabs=tabs/js-grid.json&runjavascript)

* [Cypher](https://neo4j-grapher.now.sh/?tabs=tabs/cypher.json&runcypher)

* [Cypher + Javascript](https://neo4j-grapher.now.sh/?tabs=tabs/cypher-js.json&runall)

* [Home on Gitlab Pages](https://vadim-ivlev.gitlab.io/neo4j-grapher)


## Пользовательский интерфейс 

Пользовательский интерфейс состоит из нескольких страниц/табов. Слева находятся редакторы кода  Cipher и Javascript. Правая сторона содержит представления/views:  DATA, RECORDS, GRAPH, NOTES, для просмотра данных, визуализации, или заметок.



<img src="images/neo4j-grapher-UI.png" style="max-width:1000px; width:100%;">



## Javascript

Введите следующий код в окно JavaScript и выполните его щелкнув на значке ▶ или нажав на клавиатуре CTRL-Enter. 


```javascript
let dd = {
  nodes: [
    {
      identity: "id1"
    },
    {
      identity: "id2"
    }
  ],
  links: [
    {
      start: "id1",
      end: "id2"
    }
  ]
};


createGraph(dd);
```


Функция `createGraph(data)` строит граф и показывает его на закладке GRAPH. Функция принимает объект с полями  `nodes` и `links`, содержащих массивы узлов и связей. Название полей `identity`, `start` и `end` могут быть заменены на другие. В этом случае поменяйте их в параметрах отображения графа.



<img src="images/field-names.png" style="max-width:300px;" >


Показать другую закладку можно вызвав функцию  show(VIEW_NAME). Например вызовите show(‘NOTES’) чтобы показать заметки.

Для отладки  кода откройте консоль браузера.  Чтобы добавить точку останова добавьте выражение debugger; в ваш код.


### Другие функции



*   `runCypher()` - Executes code in Cypher editor. async 
*   `neo4j.run(cypher_query)` - executes Cypher query given as a string. async 
*   `autoCreateGraph(data)` - Creates a graph from data returned by a Cypher query. async
*   `setData(data)` - Sets value of DATA global variable


**Пример**

* [Javascript basic](https://neo4j-grapher.now.sh/?tabs=tabs/js-basic.json&runjavascript)


## Cypher

Данные для построения графов можно получать из базы данных Neo4j. 

<br>

**Запросы без JavaScript**

**Если окно JavaScript закрыто**  программа пытается выполнить запрос  и показать его результат в графическом виде. 

Когда Neo4j возвращает объекты структурно похожие на узлы или связи между узлами, программа сделает дополнительные запросы к базе данных, чтобы построить связный граф. 

Например, если вы подсоединены к базе данных трансляций,  запрос Cypher


```sql
MATCH (b)-[r]-(q) 
RETURN b,q
LIMIT 200
```
выдаст

<img src="images/graph2.png" style="max-width:300px" >


**Пример**

* [Cypher](https://neo4j-grapher.now.sh/?tabs=tabs/cypher.json&runcypher)


Окно Cypher может содержать несколько запросов разделенных точкой с запятой. Результат выполнения последнего запроса сохраняется в глобальной переменной DATA. Данные можно посмотреть на закладках DATA и RECORDS.

<br>

**Запросы к Neo4j с помощью Java Script**

Язык запросов Cypher достаточно гибок, чтобы сформировать данные в формате требуемом функцией  createGraph(data). Например введите в окно Cypher следующий запрос


```
MATCH (b:Broadcast {id:354})-[r]-(q) 
RETURN {
 links: collect(r),
 nodes: collect(distinct b) + collect(distinct q)
}
```


Выполните его нажав CTRL-Enter 

В окне Javascript введите и выполните код. 


```javascript
createGraph(DATA.records[0]._fields[0])
```


Результат должен быть похож на

<img src="images/graph.png" style="max-width:250px;">

**Пример:**

* [Cypher + Javascript](https://neo4j-grapher.now.sh/?tabs=tabs/cypher-js.json&runall)



## Экспорт/импорт


С помощью  меню ☰ вы можете  переименовать, импортировать или экспортировать табы или данные отдельных табов.


## URL. Параметры приложения. 



*   `tabs` - url табов для загрузки в приложение. Url должен указывать на файл сохраненный с помощью меню  ☰ .  Сервер на котором табы должен допускать кроссдоменные запросы. 
*   `runcypher` - запустить код Cypher активного таба после загрузки табов
*   `runjavascript` - запустить код Javascript активного таба после загрузки табов
*   `runall` - запустить весь код активного таба после загрузки табов

**Пример:**

* [Cypher + Javascript](https://neo4j-grapher.now.sh/?tabs=tabs/cypher-js.json&runall)





## Известные проблемы 

Иногда на компьютерах Mac происходит ошибка инициализации 3D графики. Это зависит от видеокарты и других параметров компьютера. В этом случае попробуйте открыть приложение в другом браузере, например Firefox или Safari. Также можно попробовать уменьшить размер окна приложения и перезагрузить страницу. 
