
# Neo4j-grapher


*Represent a graph data structure in a 3D or 2D space*

 One picture is worth a thousand words. Especially when it comes to graph theory.


**Examples**


* [Home](https://neo4j-grapher.now.sh/)
  
* [Javascript basic](https://neo4j-grapher.now.sh/?tabs=tabs/js-basic.json&runjavascript)

* [Javascript grid](https://neo4j-grapher.now.sh/?tabs=tabs/js-grid.json&runjavascript)

* [Cypher](https://neo4j-grapher.now.sh/?tabs=tabs/cypher.json&runcypher)

* [Cypher + Javascript](https://neo4j-grapher.now.sh/?tabs=tabs/cypher-js.json&runall)

* [Home on Gitlab Pages](https://vadim-ivlev.gitlab.io/neo4j-grapher)




## User interface

On the left side of each tab there are the Cipher and Javascript code editors. The right side contains views: DATA, RECORDS, GRAPH, NOTES, for exploring data and taking notes.



<img src="images/neo4j-grapher-UI.png" style="max-width:1000px; width:100%;">



## Javascript

Enter the following code into the JavaScript window and execute it by clicking on the ▶ icon or by pressing CTRL-Enter on the keyboard.

```javascript
let data = {
  nodes: [
    {
      identity: "id1"
    },
    {
      identity: "id2"
    }
  ],
  links: [
    {
      start: "id1",
      end: "id2"
    }
  ]
};

createGraph(data);
```
**See in action**: 

* [Javascript basic](https://neo4j-grapher.now.sh/?tabs=tabs/js-basic.json&runjavascript)

* [Javascript grid](https://neo4j-grapher.now.sh/?tabs=tabs/js-grid.json&runjavascript)


`createGraph (data)` function builds a graph and switches to `GRAPH` view. The function accepts an object with the fields `nodes` and` links` containing arrays of nodes and links. The field names `identity`,` start` and `end` can be changed. In this case, change them accordingly in the graph options.


<img src="images/field-names.png" style="max-width:300px;" >


To switch to another view call  `show(VIEW_NAME)` function. For example, call show(‘NOTES’) to show notes.

To debug the code, open the browser console. To add a breakpoint, insert a `debugger;` expression into your code.


### Other functions



*   `runCypher()` - Executes code in Cypher editor. async 
*   `neo4j.run(cypher_query)` - executes Cypher query given as a string. async 
*   `autoCreateGraph(data)` - Creates a graph from data returned by a Cypher query. async
*   `setData(data)` - Sets value of DATA global variable


## Cypher

Graph data can be obtained from the Neo4j database. 


**Notes**

Running the Cypher for the first time may take a few minutes. 
Starting a virtual machine takes some time. Please be patient.


<br>

**No JavaScript requests**

**If the JavaScript window is closed** the program tries to execute the Cypher query and to show result in graphical form.

When Neo4j returns objects that look similar to nodes or links, the program sends additional queries to the database trying to build a linked graph.

For example, a Cypher query


```sql
MATCH (b)-[r]-(q) 
RETURN b,q
LIMIT 200
```
produces the following graph

<img src="images/graph2.png" style="max-width:300px" >

**See in action**: 
  [Cypher](https://neo4j-grapher.now.sh/?tabs=tabs/cypher.json&runcypher)


The Cypher window may contain several queries separated by a semicolon. The result of the last query is stored in the global DATA variable. Data can be viewed on the DATA and RECORDS views.

<br>

**Requests to Neo4j using JavaScript**

The Cypher query language is flexible enough to generate data in the format required by the `createGraph(data)` function. For example, enter the following query in the Cypher window and run it by pressing CTRL-Enter.


```
MATCH (b:Broadcast {id:354})-[r]-(q) 
RETURN {
 links: collect(r),
 nodes: collect(distinct b) + collect(distinct q)
}
```



Type in the following code in the Javascript window and execute the code.


```javascript
createGraph(DATA.records[0]._fields[0])
```


The result should look similar to

<img src="images/graph.png" style="max-width:250px;">

**See in action**: 
 [Cypher + Javascript](https://neo4j-grapher.now.sh/?tabs=tabs/cypher-js.json&runall)

## Export / Import


Using ☰ menu, you can rename, import, and export tabs or data from individual tabs.

## URL parameters


* `tabs` - url to load previously exported tabs from Internet. The server on which the tabs are located must allow cross-domain requests.
* `runcypher` - run the Cypher code of the active tab after loading the tabs
* `runjavascript` - run Javascript code of the active tab after loading the tabs
* `runall` - run all active tab code after loading tabs
  
Example:
* <https://neo4j-grapher.now.sh/?tabs=tabs/js-basic.json&runjavascript>


## Known Issues

Sometimes Chrome browsers on Mac computers fail to initialize 3D graphics. The behavior depends on the video card and other hardware properties. If that happens, try opening the application with other browsers, such as Firefox or Safari. You can also try to make the browser window smaller and reload the page.
