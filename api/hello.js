const fs = require('fs');
const path = require('path');

module.exports = (req, res) => {
    const { name = 'World' } = req.query
    let readText0 = 'b'
    // readText0 = fs.readFileSync('api/my.txt','utf8')
    // fs.writeFileSync('api/my.txt', `Hello ${name}!`)
    let readText = fs.readFileSync('api/my.txt','utf8')
    
    let p = path.resolve(__dirname, 'my.txt')


    res.status(200).send('my.txt: '+readText0+' '+ readText + ' path='+ p)
}