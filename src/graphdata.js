// isDataCorrect returns true if data is in correct format: {nodes:[],links:[]}
export function isDataCorrect(data){
    if (!data) {
        console.warn("No data")
        return false
    } else  if (!data.nodes || !(data.nodes instanceof Array)) {
        console.warn("No data.nodes array")
        return false
    }else if (!data.links || !(data.links instanceof Array)) {
        console.warn("No gData.links array")
        return false
    } 
    return true
}


// generateGraphData tries to generate graphData from DATA.records
export async function generateGraphData(neo4j,DATA) {
    // check id data exists 
    if (!DATA || !DATA.records || !DATA.records[0] || !DATA.records[0]._fields || !DATA.records[0]._fields[0] ) return
    
    var records = DATA.records
    var firstElem = records[0]._fields[0] 
    
    // if (!grapher) return null
    // if the first element of the first record has correct data, create a graph
    if (isDataCorrect(firstElem)) {
        console.log("Data is correct format! No additional queries to the database.")
        // createGraph(firstElem)
        return firstElem
    }  
    let nl0 = gatherNodesAndLinks(records)
    if (nl0.nodes.size == 0 && nl0.links.size == 0 ) return null

    let nl1 = await getLinksOfNodes(neo4j,[...nl0.nodes.keys()])
    let nl2 = await getNodesOfLinks(neo4j,[...nl0.links.keys()])
    // console.log("nl0=", nl0)
    // console.log("nl1=", nl1)
    // console.log("nl2=", nl2)

    mergeMaps(nl0.nodes, nl1.nodes)
    mergeMaps(nl0.links, nl1.links)


    mergeMaps(nl0.nodes, nl2.nodes)
    mergeMaps(nl0.links, nl2.links)
    
    // console.log("nl0=", nl0)
    return {
        nodes: [...nl0.nodes.values()], 
        links: [...nl0.links.values()],
        }
}


// gatherNodesAndLinks gather  nodes and links in all records
function gatherNodesAndLinks(records) {
    var nodes = new Map()
    var links = new Map()

    for (let rec of records) {
        let fields = rec._fields
        if (!fields) continue
        for (let f of fields){
            let id = f.identity
            if (typeof id == 'undefined' ) continue
            if (typeof f.start == 'undefined') {
                nodes.set(id,f)
            } else {
                links.set(id,f)
            }
        }
    }
    return { nodes, links }
}


async function getLinksOfNodes(neo4j, nodeIDs) {
    if (!nodeIDs || nodeIDs.length == 0) 
        return { links: new Map(), nodes: new Map()}
    var ids = JSON.stringify(nodeIDs)
    let query = `
    MATCH (a)-[r]-(b) WHERE 
    id(a) IN ${ids} AND id(b) IN ${ids}
    RETURN  DISTINCT r
    `
    // console.log("query=", query)
    let data = await neo4j.run(query)
    // console.log("data=", data)
    let nl = gatherNodesAndLinks(data.records)
    // console.log("additional nl=", nl)
    return nl
}


async function getNodesOfLinks(neo4j, linksIDs) {
    if (!linksIDs || linksIDs.length == 0) 
        return { links: new Map(), nodes: new Map()}

    var ids = JSON.stringify(linksIDs)
    let query = `
    MATCH (a)-[r]-(b) WHERE 
    id(r) IN ${ids} 
    RETURN  a,b
    `
    // console.log("query=", query)
    let data = await neo4j.run(query)
    // console.log("data=", data)
    let nl = gatherNodesAndLinks(data.records)
    // console.log("additional nl=", nl)
    return nl
}


function mergeMaps(m1, m2) {
    for (let k2 of m2.keys()) {
        if (!m1.has(k2))
            m1.set(k2,m2.get(k2))
    }
    return m1
}


export async function getAdjacentNodes(neo4j, nodeIDs) {
    if (!nodeIDs || nodeIDs.length == 0 || !nodeIDs[0]) 
        return { links: new Map(), nodes: new Map()}
    var ids = JSON.stringify(nodeIDs)
    let query = `
    MATCH (a)-[r]-(b) WHERE 
    id(a) IN ${ids} 
    RETURN a, r, b
    `
    // console.log("query=", query)
    let data = await neo4j.run(query)
    // console.log("data=", data)
    let nl = gatherNodesAndLinks(data.records)
    // console.log("additional nl=", nl)
    // return nl

    return {
        nodes: [...nl.nodes.values()], 
        links: [...nl.links.values()],
    }

}

// adds missing elements from d1 to d0
export function addMissingGraphDataElements(d0, d1) {
    if (!d0 || !d1 ) return
    var count = 0
    count += addMissingArrayElements(d0.nodes, d1.nodes)
    count += addMissingArrayElements(d0.links, d1.links)
    return count
}

// adds missing array elements from a1 to a0
function addMissingArrayElements(a0, a1) {
    if (!a0 || !a1 ) return

    // build set of ids of a0 array
    var ids = new Set()
    for (let e0 of a0) {
        ids.add(e0.identity)
    }
    let count = 0
    //add absent elements from a1 to a0
    for (let e1 of a1) {
        let id1 = e1.identity
        if (ids.has(id1)) continue
        ids.add(id1)
        count ++
        // add new element!
        a0.push(e1)
    }
    return count
}

export function removeNodeByID(data, id) {
    // for (let n of data.nodes){
    let nodes = data.nodes
    for( let i = 0; i < nodes.length; i++) {   
        if (nodes[i].identity == id) {
            nodes.splice(i, 1)
            break
        }
    }

    let links = data.links
    for( let i = 0; i < links.length; i++) {   
        if (links[i].start == id || links[i].end == id) {
            links.splice(i, 1)
            i--
        }
    }
}
